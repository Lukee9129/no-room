﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Forcefield : MonoBehaviour
{
    public Transform player;
    public float lasts;
    private float scale = 2;
    private float timeInUse = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Globals.hasForcefield)
        {
            transform.position = player.position;
            timeInUse += Time.deltaTime;
            if (timeInUse > lasts)
            {
                scale-= 0.01f;
                transform.localScale = new Vector3(scale, scale, scale);
            }
            if (scale <= 0.01)
            {
                Globals.hasForcefield = false;
                Destroy(gameObject);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        string name = collision.gameObject.name;
        if (name.Contains("Player"))
        {
            //pickup
            Globals.hasForcefield = true;
        } else
        {
            if (name.Contains("Clone") && (name.Contains("Zombie") || name.Contains("Hoarder") || name.Contains("Strafer")))
            {
                //kill zombie
            }
        }
    }
}
