﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Exit : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter2D(Collider2D collision)
    {      
        if (collision.gameObject.name.Contains("Player"))
        {
            Debug.Log("Winner!");
            collision.gameObject.transform.localScale = new Vector3(0, 0, 0);
            Globals.isImmortal = true;
            SceneManager.LoadScene("Winner", LoadSceneMode.Single);
        }
    }
}
