﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Uzi : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Contains("Player"))
        {
            Globals.hasUzi = true;
            GameObject.Find("Uzi_Frame").transform.localScale = new Vector3(1, 1, 1);
            Destroy(gameObject);
        }
    }
}
