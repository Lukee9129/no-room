﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unlock : MonoBehaviour
{
    public Texture2D buyCursor;
    public Texture2D defaultCursor;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    private void OnMouseOver()
    {
        if (Globals.score >= Globals.Cost)
            Cursor.SetCursor(buyCursor, hotSpot, cursorMode);
        if (Input.GetMouseButtonDown(1))
            if (Globals.score >= Globals.Cost)
            {
                //Unlocked
                Globals.unlocked++;
                Globals.score -= Globals.Cost;
                Globals.Cost += 25 + (25 * (Globals.unlocked + 1)); //This should increase over time
                Cursor.SetCursor(defaultCursor, hotSpot, cursorMode);
                GameObject.Find("Score").BroadcastMessage("refreshScore");
                GameObject.Find("Cost").BroadcastMessage("refreshCost");
                GameObject.Find("Locked").GetComponent<AudioSource>().Play();
                Destroy(gameObject);
            }
    }
    void OnMouseEnter()
    {
        if (Globals.score >= Globals.Cost)
            Cursor.SetCursor(buyCursor, hotSpot, cursorMode);
    }

    void OnMouseExit()
    {
        Cursor.SetCursor(defaultCursor, hotSpot, cursorMode);
    }
}
