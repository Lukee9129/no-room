﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Globals
{
    public static int cost = 25;
    public static int score = 0;
    public static float timeAlive = 0;
    public static int unlocked = 0;
    public static int zombiesKilled = 0;
    public static int strafersKilled = 0;
    public static int hoardersKilled = 0;
    public static int weapon = 1;
    public static bool hasShotgun = false;
    public static bool hasUzi = false;
    public static bool isHoarderActive = false;
    public static bool isHoarderHoarding = false;
    public static bool isImmortal = false;
    public static bool hasForcefield = false;
    
    public static int Cost
    {
        get { return cost; }
        set { cost = value > 500 ? 500 : value; }
    }
    public static void resetGlobals()
    {
        cost = 25;
        score = 0;
        timeAlive = 0;
        unlocked = 0;
        zombiesKilled = 0;
        strafersKilled = 0;
        hoardersKilled = 0;
        weapon = 1;
        hasShotgun = false;
        hasUzi = false;
        isHoarderActive = false;
        isHoarderHoarding = false;
        isImmortal = false;
        hasForcefield = false;
    }
}
