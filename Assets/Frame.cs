﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Frame : MonoBehaviour
{
    public Texture2D frameOn;
    public Texture2D frameOff;
    public int weaponNumber;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Globals.weapon == weaponNumber)
        {
            GetComponent<Image>().sprite = Sprite.Create(frameOn, new Rect(0, 0, frameOn.width, frameOn.height), new Vector2(0.5f, 0.5f));
        } else
        {
            GetComponent<Image>().sprite = Sprite.Create(frameOff, new Rect(0, 0, frameOff.width, frameOff.height), new Vector2(0.5f, 0.5f));
        }
    }
}
