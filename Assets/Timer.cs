﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Text text;
    // Update is called once per frame
    void Update()
    {
        if (!Globals.isImmortal)
        {
            Globals.timeAlive += Time.deltaTime;
        }
        text.text = "Time spent alive: " + Globals.timeAlive.ToString("0.00");
    }
}
