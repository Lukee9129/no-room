﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Winner : MonoBehaviour
{
    public Text text;
    // Start is called before the first frame update
    void Start()
    {
        text.text += "\nTime spent alive: " + Globals.timeAlive.ToString("0.00") + "s";
        text.text += "\nZombies Killed: " + Globals.zombiesKilled;
        text.text += "\nStrafers Killed: " + Globals.strafersKilled;
        text.text += "\nHoarders Killed: " + Globals.hoardersKilled;
        text.text += "\nDoors opened: " + Globals.unlocked;
        text.text += "\nTotal Score: " + (Globals.unlocked * 500 + Globals.hoardersKilled * 200 + Globals.strafersKilled * 100 * Globals.zombiesKilled * 25 + Globals.score + (int)Globals.timeAlive);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Globals.resetGlobals();
            SceneManager.LoadScene("Main", LoadSceneMode.Single);
        }
    }
}
