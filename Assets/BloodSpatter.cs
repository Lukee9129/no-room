﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodSpatter : MonoBehaviour
{
    public float speed = 25f;
    public float lasts = 5;
    private float actualSpeed = 0f;
    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.name.Contains("Clone"))
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, 9);
            transform.localScale = new Vector3(Random.Range(5f, 10f), Random.Range(1f, 10f), 1f);
            Destroy(gameObject, Random.Range(0.1f, lasts));
            actualSpeed = Random.Range(5f, speed * 5);
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        if (gameObject.name.Contains("Clone"))
        {
            GetComponent<Rigidbody2D>().velocity = transform.up * actualSpeed;
        }
        //.AddForce(transform.forward * speed);
    }
}
