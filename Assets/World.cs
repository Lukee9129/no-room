﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour
{
    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    public GameObject coins;
    void Start()
    {
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
        Random.InitState(System.DateTime.Now.Millisecond);
        for (int i = 0; i < 40; ++i)
        {
            Instantiate(coins, new Vector3(Random.Range(-6f, -9f), Random.Range(3f, 7f), 11), transform.rotation);
        }
        for (int i = 0; i < 200; ++i)
        {
            Instantiate(coins, new Vector3(Random.Range(9f, 25f), Random.Range(-37.25f, -36.7f), 11), transform.rotation);
        }
    }

    void Update()
    {

    }

    // void OnMouseEnter()
    // {
    //    Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
    //}

    //    void OnMouseExit()
    //    {
    //        Cursor.SetCursor(null, Vector2.zero, cursorMode);
    //    }
}
