﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Currency : MonoBehaviour
{
    public GameObject player;
    private float time = 0;
    private int delay = 2;
    private Rigidbody2D rb;
    private bool ranOnce = false;
    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.name.Contains("Clone"))
        {
            rb = GetComponent<Rigidbody2D>();
            rb.velocity = Vector3.zero;
            transform.position = new Vector3(transform.position.x, transform.position.y, 9);
        }
    }


    // Update is called once per frame
    void Update()
    {

    }
    private void FixedUpdate()
    {
        if (!gameObject.name.Contains("Stationary") && gameObject.name.Contains("Clone"))
        {
            if (!ranOnce)
            {
                rb.AddRelativeForce(new Vector3(100, 100));
                ranOnce = true;
            }
            time += Time.deltaTime;
            if (time > delay)
            {
                if (rb != null)
                    rb.AddForce((player.transform.position - transform.position) * 5);
            }
        }
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (gameObject.name.Contains("Clone") && col.gameObject.name.Contains("Player"))
        {
            Destroy(gameObject);
            GameObject.Find("Score").BroadcastMessage("addScore");
        }
    }
    private void LateUpdate()
    {
    }
}
