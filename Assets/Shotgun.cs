﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Contains("Player"))
        {
            Globals.hasShotgun = true;
            GameObject.Find("Shotgun_Frame").transform.localScale = new Vector3(1, 1, 1);
            Destroy(gameObject);
        }
    }
}
