﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 1f;
    public float lasts = 5;
    private float startTime;
    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.name.Contains("Clone"))
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, 8);
            startTime = Time.deltaTime;
            if (Globals.weapon == 2)
            {
                lasts = Random.Range(0.1f, 0.5f);
            } else if (Globals.weapon == 3)
            {
                lasts = Random.Range(0.3f, lasts);
            }
            Destroy(gameObject, lasts);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
           
    }

    void FixedUpdate()
    {
        if (gameObject.name.Contains("Clone"))
        {
            GetComponent<Rigidbody2D>().velocity = transform.up * speed;
        }
        //.AddForce(transform.forward * speed);
    }
}
