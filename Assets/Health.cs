﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public Texture2D healthFull;
    public Texture2D healthLow;
    public Texture2D healthLower;
    public Texture2D healthEmpty;
    private int health = 2;
    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.name.Contains("Clone"))
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, 7);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    public void minusHealth()
    {
        SpriteRenderer sr =  GetComponent<SpriteRenderer>();
        switch (health--)
        {
            case 2:
                sr.sprite = Sprite.Create(healthLow, new Rect(0, 0, healthLow.width, healthLow.height), new Vector2(0.5f, 0.5f));
                break;
            case 1:
                sr.sprite = Sprite.Create(healthLower, new Rect(0, 0, healthLower.width, healthLower.height), new Vector2(0.5f, 0.5f));
                break;
            case 0:
                sr.sprite = Sprite.Create(healthEmpty, new Rect(0, 0, healthEmpty.width, healthEmpty.height), new Vector2(0.5f, 0.5f));
                Destroy(gameObject, 1);
                break;
        }
    }
    public void dead()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        sr.sprite = Sprite.Create(healthEmpty, new Rect(0, 0, healthEmpty.width, healthEmpty.height), new Vector2(0.5f, 0.5f));
        Destroy(gameObject, 1);
    }

    Quaternion rotation;
    void Awake()
    {
        rotation = transform.rotation;
    }
    void LateUpdate()
    {
        transform.rotation = rotation;
    }
}
