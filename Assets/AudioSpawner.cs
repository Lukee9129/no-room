﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSpawner : MonoBehaviour
{
    public AudioClip[] audioClips;
    private int randomMusic = 0;
    private float randomTime = 0f;
    private float timeCounter = 0.0f;
    private AudioSource audioSource;
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }
    void Update()
    {       
        
        if (timeCounter > randomTime && !audioSource.isPlaying)
        {
            randomTime = Random.Range(5f, 10f);
            timeCounter = 0.0f;
            audioSource.Stop();
            ChooseMusic();
            audioSource.Play();
        }

        timeCounter += Time.deltaTime;
    }

    void ChooseMusic()
    {
        randomMusic = Random.Range(0, 3);

        switch (randomMusic)
        {
            case 0:
                audioSource.clip = audioClips[0];
                break;
            case 1:
                audioSource.clip = audioClips[1];
                break;
            case 2:
                audioSource.clip = audioClips[2];
                break;
        }
    }
}
