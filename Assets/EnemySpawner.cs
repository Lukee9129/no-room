﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject zombie;
    public GameObject strafer;
    public GameObject hoarder;
    private float zombieDelay = 0;
    private float sinceLastZombie = 0;
    private GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player_PA");
        //Start with 3 zombies (non-random)
        Instantiate(zombie, new Vector3(-32.6f, -18.9f), transform.rotation);
        Instantiate(zombie, new Vector3(-33f, 0), transform.rotation);
        Instantiate(zombie, new Vector3(35f, 8f), transform.rotation);
        //and 5 more further away (more random)
        Random.InitState(System.DateTime.Now.Millisecond);
        Instantiate(zombie, new Vector3(Random.Range(-50, -80), Random.Range(-50, -80)), transform.rotation);
        Instantiate(zombie, new Vector3(Random.Range(50, 80), Random.Range(-50, -80)), transform.rotation);
        Instantiate(zombie, new Vector3(Random.Range(-50, -80), Random.Range(50, 80)), transform.rotation);
        Instantiate(zombie, new Vector3(Random.Range(50, 80), Random.Range(50, 80)), transform.rotation);
        Instantiate(zombie, new Vector3(Random.Range(-50, -80), Random.Range(-50, -80)), transform.rotation);
        //Spawn a couple strafers
        Instantiate(strafer, new Vector3(Random.Range(-300, 300), -150f), transform.rotation);
        Instantiate(strafer, new Vector3(Random.Range(-300, 300), 150f), transform.rotation);
    }

    // Update is called once per frame
    void Update()
    {

        if (sinceLastZombie > zombieDelay)
        {
            Random.InitState(System.DateTime.Now.Millisecond);
            int whatToSpawn = (int)Random.Range(1, 15);
            if (Globals.zombiesKilled % 5 == 0 && !Globals.isHoarderActive)
            {
                //Every 5 zombies, spawn a hoarder if one isn't active
                Globals.isHoarderActive = true;
                whatToSpawn = 0;
            }
            int position = (int)Random.Range(1, 4);
            GameObject spawn = null;
            if (whatToSpawn == 1)
            {
                spawn = strafer;
            } else if (whatToSpawn == 0)
            {
                spawn = hoarder;
            }
            else
            {
                spawn = zombie;
            }
            //Progressively spawn zombies outside the playarea
            switch (position)
            {
                case 1:
                    Instantiate(spawn, new Vector3(Random.Range(-300, 300), -300f), transform.rotation);
                    break;
                case 2:
                    Instantiate(spawn, new Vector3(Random.Range(-300, 300), 300f), transform.rotation);
                    break;
                case 3:
                    Instantiate(spawn, new Vector3(300f, Random.Range(-300, 300)), transform.rotation);
                    break;
                case 4:
                    Instantiate(spawn, new Vector3(-300f, Random.Range(-300, 300)), transform.rotation);
                    break;
            }
            zombieDelay = 5;
            if (Globals.unlocked > 5)
            {
                zombieDelay = Random.Range(0.25f, 1f);
            }
            else if (Globals.strafersKilled >= 2 || Globals.zombiesKilled > 8)
            {
                zombieDelay = Random.Range(1, 4);
            }
            else if (Globals.zombiesKilled > 20)
            {
                zombieDelay = Random.Range(1, 3);
            } else if (Globals.zombiesKilled > 100)
            {
                zombieDelay = Random.Range(1, 2);
            }
            else
            {
                zombieDelay = Random.Range(1, 5);
            }
            sinceLastZombie = 0;
        }

        sinceLastZombie += Time.deltaTime;
    }
}
