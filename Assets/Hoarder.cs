﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hoarder : MonoBehaviour
{
    public GameObject health;
    public GameObject blood;
    public Transform target;
    public float rotateSpeed;
    public float offset;
    public float speed;
    public int value = 25;
    public GameObject currency;
    public GameObject lockedZone;
    private GameObject myHealth;
    private Rigidbody2D rb;
    private int remainingHealth = 10;
    private float delayBeforeCharging = 15;
    private float timeSinceStopped = 0;
    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.name.Contains("Clone"))
        {
            rb = GetComponent<Rigidbody2D>();
            myHealth = Instantiate(health, transform);
            transform.position = new Vector3(transform.position.x, transform.position.y, 1);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.name.Contains("Clone"))
        {
            Vector3 vectorToTarget = target.position - transform.position;
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg + offset;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * rotateSpeed);
            myHealth.transform.position = transform.position + Vector3.down * 1;
        }
    }

    void FixedUpdate()
    {
        if (gameObject.name.Contains("Clone"))
        {
            if (timeSinceStopped > delayBeforeCharging)
            {
                Globals.isHoarderHoarding = false;
                rb.bodyType = RigidbodyType2D.Dynamic;
                rb.AddRelativeForce(Vector3.up * speed);
            }
            else { 
                float dist = Vector2.Distance(transform.position, target.position);
                Globals.isHoarderHoarding = true;
                if (dist > 60)
                {
                    rb.AddRelativeForce(Vector3.up * 100);
                }
                else
                {
                    rb.bodyType = RigidbodyType2D.Static;
                    timeSinceStopped += Time.deltaTime;
                }
            }
        }
            
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col != null && gameObject.name.Contains("Clone"))
        {
            bool touchedForcefield = false;
            if (col.gameObject.name.Contains("Forcefield") && Globals.hasForcefield)
            {
                touchedForcefield = true;
            }
            if (touchedForcefield || (col.gameObject.name.Contains("Bullet") && col.gameObject.name.Contains("Clone")))
            {
                if (touchedForcefield)
                {
                    remainingHealth = 1;
                    myHealth.BroadcastMessage("dead");
                }
                else
                {
                    //We've been shot! :O
                    Destroy(col.gameObject);
                    rb.AddRelativeForce(Vector3.down * speed);
                    if (remainingHealth == 7 || remainingHealth == 4 || remainingHealth == 1)
                    {
                        myHealth.BroadcastMessage("minusHealth");
                    }
                }
                if (--remainingHealth == 0)
                {
                    Random.InitState(System.DateTime.Now.Millisecond);
                    for (int i = 0; i < value; ++i)
                    {
                        GameObject childCurrency = Instantiate(currency, transform.position, Quaternion.Euler(0, 0, Random.Range(0, 360f)));
                        childCurrency.transform.parent = null;
                    }
                    for (int i = 0; i < Random.Range(50, 100); ++i)
                    {
                        GameObject childBlood = Instantiate(blood, transform.position, transform.rotation * Quaternion.Euler(0, 0, 180 + Random.Range(-15, 15)));
                        childBlood.transform.parent = null;
                    }
                    //We died :'(
                    Globals.isHoarderActive = false;
                    Globals.isHoarderHoarding = false;
                    Globals.hoardersKilled++;
                    myHealth.transform.parent = null;
                    Destroy(gameObject);
                }
            }
        }
    }
}
