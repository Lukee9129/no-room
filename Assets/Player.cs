﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Player : MonoBehaviour
{
    public GameObject bullet;
    public GameObject blood;
    public AudioClip glockShot;
    public AudioClip shotgunShot;
    public AudioClip uziShot;
    public AudioSource audioSelf;
    public Transform shotSpawn;
    public float rotationSpeed;
    public float forwardSpeed;
    public float backwardsSpeed;
    public float offset = 0;
    Rigidbody2D rb;
    private float shotgunDelay = 1;
    private float timeSinceLastShot = 1;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
        Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg + offset;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationSpeed * Time.deltaTime);
        timeSinceLastShot += Time.deltaTime;

        if (Globals.weapon == 3)
        {
            if (Input.GetMouseButton(0))
            {
                audioSelf.clip = glockShot;
                audioSelf.loop = true;
                //Uzi
                if (timeSinceLastShot > 0.05)
                {
                    Instantiate(bullet, shotSpawn.position, shotSpawn.rotation * Quaternion.Euler(0, 0, Random.Range(-15, 15)));
                    rb.AddRelativeForce(Vector3.down * 1);
                    timeSinceLastShot = 0;
                    audioSelf.Play();
                }
            }
        } else {
            if (Input.GetMouseButtonDown(0))
            {
                if (Globals.weapon == 2)
                {
                    //Shotty  
                    if (timeSinceLastShot > shotgunDelay)
                    {
                        audioSelf.clip = shotgunShot;
                        Instantiate(bullet, shotSpawn.position, shotSpawn.rotation * Quaternion.Euler(0, 0, Random.Range(-15, 15)));
                        Instantiate(bullet, shotSpawn.position, shotSpawn.rotation * Quaternion.Euler(0, 0, Random.Range(-15, 15)));
                        Instantiate(bullet, shotSpawn.position, shotSpawn.rotation * Quaternion.Euler(0, 0, Random.Range(-15, 15)));
                        Instantiate(bullet, shotSpawn.position, shotSpawn.rotation * Quaternion.Euler(0, 0, Random.Range(-15, 15)));
                        Instantiate(bullet, shotSpawn.position, shotSpawn.rotation * Quaternion.Euler(0, 0, Random.Range(-15, 15)));
                        Instantiate(bullet, shotSpawn.position, shotSpawn.rotation * Quaternion.Euler(0, 0, Random.Range(-15, 15)));
                        Instantiate(bullet, shotSpawn.position, shotSpawn.rotation * Quaternion.Euler(0, 0, Random.Range(-15, 15)));
                        Instantiate(bullet, shotSpawn.position, shotSpawn.rotation);
                        timeSinceLastShot = 0;
                        rb.AddRelativeForce(Vector3.down * 500);
                        audioSelf.Stop();
                        audioSelf.Play();
                    }
                }
                else
                {
                    //Pistol
                    audioSelf.clip = glockShot;
                    audioSelf.Stop();
                    audioSelf.Play();
                    Instantiate(bullet, shotSpawn.position, shotSpawn.rotation);
                    timeSinceLastShot = 0;
                }
            }
        }
        if (Input.GetKey(KeyCode.W))
        {
            // Forward
            rb.AddForce(Vector3.up * forwardSpeed); //TODO: Use % between forwardSpeed and backwardsSpeed depending on faced direction
        }
        if (Input.GetKey(KeyCode.A))
        {
            //Left
            rb.AddForce(Vector3.left * forwardSpeed);

        }
        if (Input.GetKey(KeyCode.S))
        {
            //Backwards
            rb.AddForce(Vector3.down * forwardSpeed);
        }
        if (Input.GetKey(KeyCode.D))
        {
            //Right
            rb.AddForce(Vector3.right * forwardSpeed);
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            Globals.isImmortal = !Globals.isImmortal;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            Globals.resetGlobals();
            SceneManager.LoadScene("Main", LoadSceneMode.Single);
        }
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Globals.weapon = 1;
        } else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            if (Globals.hasShotgun)
            {
                Globals.weapon = 2;
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            if (Globals.hasUzi)
            {
                Globals.weapon = 3;
            }
        }
    }
    float AngleBetweenPoints(Vector2 a, Vector2 b) {
         return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
     }
    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (!Globals.isImmortal)
        {
            string name = collision.gameObject.name;
            if (name.Contains("Clone") && (name.Contains("Zombie") || name.Contains("Hoarder") || name.Contains("Strafer")))
            {
                //WE DIE  
                for (int i = 0; i < Random.Range(100, 150); ++i)
                {
                    GameObject childBlood = Instantiate(blood, transform.position, transform.rotation * Quaternion.Euler(0, 0, Random.Range(-360, 360)));
                    childBlood.transform.parent = null;
                }
                gameObject.transform.localScale = new Vector3(0, 0, 0);
                rb.bodyType = RigidbodyType2D.Static;
                Globals.isImmortal = true;
            }
        }
    }
}
