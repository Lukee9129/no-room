﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : MonoBehaviour
{
    public GameObject health;
    public GameObject blood;
    public string hoarderName;
    public Transform target;
    public float rotateSpeed;
    public float offset;
    public float speed;
    public int value = 25;
    public GameObject currency;
    public GameObject lockedZone;
    private GameObject myHealth;
    private Rigidbody2D rb;
    private int remainingHealth = 3;
    private bool isDistracted = false;
    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.name.Contains("Clone"))
        {
            rb = GetComponent<Rigidbody2D>();
            myHealth = Instantiate(health, transform);
            transform.position = new Vector3(transform.position.x, transform.position.y, 1);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.name.Contains("Clone"))
        {
            GameObject hoarder = GameObject.Find(hoarderName);
            Transform tempTarget = target;
            if (hoarder != null && Globals.isHoarderHoarding)
            {
                float dist = Vector2.Distance(transform.position, hoarder.transform.position);
                float playerDist = Vector2.Distance(transform.position, target.position);
                if (dist < 50 && playerDist > 20)
                {
                    tempTarget = hoarder.transform;
                    isDistracted = true;
                } else
                {
                    isDistracted = false;
                }
            } else
            {
                isDistracted = false;
            }
            Vector3 vectorToTarget = tempTarget.position - transform.position;
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg + offset;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * rotateSpeed);
            myHealth.transform.position = transform.position + Vector3.down * 1;
        }
    }

    void FixedUpdate()
    {
        if (gameObject.name.Contains("Clone"))
        {
            float tempSpeed = speed;
            float dist = Vector2.Distance(transform.position, target.position);
            if (dist > 100 || isDistracted)
            {
                tempSpeed = 100;
            }
            rb.AddRelativeForce(Vector3.up * (tempSpeed + (Globals.zombiesKilled <= 4 ? 5 * Globals.zombiesKilled : 20)));
        }
            
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col != null && gameObject.name.Contains("Clone"))
        {
            bool touchedForcefield = false;
            if (col.gameObject.name.Contains("Forcefield") && Globals.hasForcefield)
            {
                touchedForcefield = true;
            }
            if (touchedForcefield || (col.gameObject.name.Contains("Bullet") && col.gameObject.name.Contains("Clone")))
            {
                if (touchedForcefield)
                {
                    remainingHealth = 1;
                    myHealth.BroadcastMessage("dead");
                }
                else
                {
                //We've been shot! :O
                    Destroy(col.gameObject);
                    rb.AddRelativeForce(Vector3.down * speed * 10);
                    myHealth.BroadcastMessage("minusHealth");
                }
                if (--remainingHealth == 0)
                {
                    Random.InitState(System.DateTime.Now.Millisecond);
                    for (int i = 0; i < value; ++i)
                    {
                        GameObject childCurrency = Instantiate(currency, transform.position, Quaternion.Euler(0, 0, Random.Range(0, 360f)));
                        childCurrency.transform.parent = null;
                    }
                    for (int i = 0; i < Random.Range(10, 50); ++i)
                    {
                        GameObject childBlood = Instantiate(blood, transform.position, transform.rotation * Quaternion.Euler(0, 0, 180 + Random.Range(-15, 15)));
                        childBlood.transform.parent = null;
                    }
                    //We died :'(
                    Globals.zombiesKilled++;
                    myHealth.transform.parent = null;
                    Destroy(gameObject);
                }
            }
        }
    }
}
